from flask import Flask, render_template, request, current_app, redirect, url_for, session, send_file, send_from_directory
import mysql.connector
import os
from dotenv import load_dotenv
from voti_classes import Course, MeetingInfo, SQuery, Result, Schedule
from voti_classes import ics_maker
import secrets
import math
import itertools
import datetime

# Initialize Flask application
app = Flask(__name__)

load_dotenv()

DB_HOST = os.getenv('DB_ENDPOINT')
DB_USER = os.getenv('DB_USERNAME')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')

mydb = None

app.config['SECRET_KEY'] = secrets.token_urlsafe(16)

def establish_connection():
    global mydb
    if not mydb:
        mydb = mysql.connector.connect(
            host=DB_HOST,
            user=DB_USER,
            password=DB_PASSWORD,
            port=DB_PORT,
            database=DB_NAME
        )
        print('Connected to the database')

def close_connection():
    global mydb
    if mydb:
        mydb.close()
        mydb = None
        print('Disconnected from the database')

# Define route for the home page
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/new-schedule')
def new_schedule():
    
    #build_squery(["CS 1114", "ENGL 1106", "ENGE 1216"], [], 0.5, "Spring 2024")

    #return render_template('generating_page.html')

    return render_template('new_schedule.html')

@app.route('/view-schedules')
def view_schedules():
    return 'BEEP...ERRR..BOOP...under construction...This will be the view schedule page.'

@app.route('/process-schedule', methods=['POST'])
def process_schedule():
    # Get semester from form
    session['semester'] = request.form.get('semester')
    # Collect all class inputs into a list from form, names begin with "course"
    session['classList'] = [value for key, value in request.form.items() if key.startswith('course') and value]
    print(f"Semester: { session['semester']}, Class List: {session['classList']}") # Debugg
    return redirect(url_for('block_times'))

@app.route('/block-times', methods=['GET', 'POST'])
def block_times():
    if request.method == 'POST':
        blocked_times = request.form.getlist('blockedTimes')
        serialized_block_times = []

        for block_time in blocked_times:
            day, hour, minute = block_time.split('-')
            serialized_block_times.append({
                'day_of_week': day,
                'building': "",  # Assuming building is not set in this context
                'hour': int(hour),
                'minute': int(minute)
            })

        # Store the serialized blocked times in the session
        session['blockTimes'] = serialized_block_times

        # Debug print for blocked times page
        # for obj in serialized_block_times:
        #     # Assuming you want to debug the serialization process
        #     app.logger.debug(f"Serialized Blocked times: {obj}")

        return redirect(url_for('prof_grading'))

    return render_template('blocked_times.html')


@app.route('/prof-grading', methods=['GET', 'POST'])
def prof_grading():
    if request.method == 'POST':
        # Extract the selected GPA value from the form
        selected_gpa = request.form.get('selectedGPA')
        
        # Store the selected GPA value in the session
        session['selectedGPA'] = selected_gpa
        app.logger.debug(f"GPA Selected: {selected_gpa}") # Debugg
        
        # Redirect or handle the POST request as needed
        return redirect(url_for('start_generating'))  
    
    # If it's a GET request, just render the template
    return render_template('prof_grading.html')

@app.route('/start-generating')
def start_generating():
    # Render a template that includes a meta refresh tag to '/generating-page'
    return render_template('generating_page.html')

global_res = None
@app.route('/generating-page')
def generating_page():
    global global_res
    if 'classList' in session and 'blockTimes' in session and 'selectedGPA' in session and 'semester' in session:
        class_list = session['classList']
        serialized_block_times = session['blockTimes']
        selected_gpa = session['selectedGPA']
        semester = session['semester']

        # Deserialize the blockTimes
        block_times_objects = [MeetingInfo(day_of_week=block_time['day_of_week'], 
                                           building=block_time['building'], 
                                           hour=block_time['hour'], 
                                           minute=block_time['minute']) 
                               for block_time in serialized_block_times]
        
        # Now you can build the SQuery object with deserialized data
        global_res = build_squery(class_list, block_times_objects, selected_gpa, semester)
    return redirect(url_for('show_results'))

@app.route('/show-results')
def show_results():
    global global_res
    if global_res:
        return render_template('result_page.html', result=global_res)
    else:
        return "No result available", 404


def build_squery(classList, blockTimes,newProfGPA,semester):

    query_instance = SQuery(classList, blockTimes, newProfGPA, semester)

    establish_connection()

    global mydb
    if mydb:

        cursor = mydb.cursor()

        query = """
        SELECT *
        FROM votidata.time_table
        WHERE CONCAT(TRIM(Subject), ' ', TRIM(Code)) IN ({});
        """.format(", ".join(f"'{s}'" for s in query_instance.classList))

        # print(query)

        cursor.execute(query)
        timetable = cursor.fetchall()

        professorsAndCourses = []
        profCourseGrades = []

        #Get all Professor/Course combonations.
        for row in timetable:
            professor = row[10]
            first_space_index = professor.find(' ')
            if first_space_index != -1:
                professor = professor[first_space_index+1:]

            course = row[3] + " " + row[4]
            if (professor, course) not in professorsAndCourses:
                professorsAndCourses.append((professor, course))

        #Find the GPA Estimate for each combonation.
        for profCourse in professorsAndCourses:
            profGPAQuery = """
            SELECT *
            FROM votidata.grade_distribution
            WHERE CONCAT(TRIM(subject), ' ', TRIM(course_number)) = "{}"
            AND LOWER(instructor) = LOWER("{}")
            """.format(profCourse[1], profCourse[0])

            print(profGPAQuery)

            cursor.execute(profGPAQuery)
            profResults = cursor.fetchall()

            if len(profResults) > 0:

                #The professor has taught the course before.

                sum = 0.0
                for row in profResults:
                    sum += float(row[6])

                average = sum / len(profResults)

                profCourseGrades.append((profCourse[0], profCourse[1], average))
            else:

                #the professor hasn't taught the course before

                profGPAQuery = """
                SELECT *
                FROM votidata.grade_distribution
                WHERE CONCAT(TRIM(subject), ' ', TRIM(course_number)) = '{}'
                """.format(profCourse[1], profCourse[0])

                # print(profGPAQuery)

                cursor.execute(profGPAQuery)
                courseResult = cursor.fetchall()

                gpaRows = []
                for row in courseResult:
                    gpaRows.append(row[6])
                gpaRows.sort()

                print("Type of query_instance.newProfGPA:", type(query_instance.newProfGPA))
                print("Value of query_instance.newProfGPA:", query_instance.newProfGPA)
                print(len(gpaRows))
                print(type(gpaRows[math.floor(float(query_instance.newProfGPA) * (len(gpaRows)-1))]))
                estimatedGPA = float(gpaRows[math.floor(float(query_instance.newProfGPA) * (len(gpaRows)-1))])

                profCourseGrades.append((profCourse[0], profCourse[1], estimatedGPA))

        for pcg in profCourseGrades:
            print(pcg)


            #0 2024,
            #1 'SPRING',
            #2 16762,
            #3 'ISE',
            #4 '4204',
            #5 'Production Plan Inventory Ctrl',
            #6 'LECTURE',
            #7 'Modality.IN_PERSON',
            #8 '3',
            #9 90,
            #10 'KP Ellis',
            #11 "Tuesday, {('11:00AM', '12:15PM', 'MCB 113')}",
            #12 "Thursday, {('11:00AM', '12:15PM', 'MCB 113')}",
            #13 None,
            #14 None
  
        # print(timetable[0])

        #Gets all available courses with GPA possibilies.
        courses = []
        for row in timetable:



            professor = row[10]
            first_space_index = professor.find(' ')
            if first_space_index != -1:
                professor = professor[first_space_index+1:]

            course = row[3] + " " + row[4]

            modality = ""
            if row[7] is not None:
                modality = ' '.join(word.capitalize() for word in row[7].split(".")[-1].split("_"))

            courseSemester = row[1].capitalize() + " " + str(row[0])

            meetingTimes = []
            for schedule_string in [row[11], row[12], row[13], row[14]]:
                if schedule_string is not None:

                    parts = schedule_string.split(", {(")
                    DayOfWeek = parts[0]
                    times_and_building = parts[1].split(", ")

                    startTime = times_and_building[0].strip("('")
                    endTime = times_and_building[1]
                    building = times_and_building[2].strip("')}")

                    startTimeHours, startTimeMinutes_AMPM = startTime.split(":")
                    startTimeMinutes = startTimeMinutes_AMPM[:2]

                    newTime = MeetingInfo(DayOfWeek, building, startTimeHours, startTimeMinutes)
                    print(startTimeHours)
                    print(startTimeMinutes)
                    meetingTimes.append(newTime)
                
            thisGrade = -1
            for pcg in profCourseGrades:
                if pcg[0] == professor and pcg[1] == course:
                    thisGrade = pcg[2]

            thisCourse = Course(row[5], row[3], row[4], professor, row[2], thisGrade, int(row[8]), modality, courseSemester, meetingTimes)
            courses.append(thisCourse)

        #Created 'buckets' of these classes.
        course_dict = {}
        for course in courses:
            if course.get_credits() > 0:
                if course.get_semester() == query_instance.semester:
                    if course.get_courseCode() not in course_dict.keys():
                        course_dict[course.get_courseCode()] = []
                    course_dict[course.get_courseCode()].append(course)
        
        # for key in course_dict.keys():
            # print(key + ": " + str(len(course_dict[key])) + " course options") 

        combinations = list(itertools.product(*course_dict.values()))
        
        schedules = []

        iterator = 1
        for combo in combinations:
            #This is where in the future we will have logic checks to see if a schedule is possible.

            totalCredits = 0.0
            totalGPA = 0.0
            for c in combo:
                totalGPA += float(c.get_projGPA()) * float(c.get_credits())
                totalCredits += c.get_credits()

            if totalCredits == 0:
                projectedGPA = 0
            else:
                projectedGPA = totalGPA / totalCredits

            thisSchedule = Schedule(iterator, "Schedule "  + str(iterator), projectedGPA, combo, None, None, math.floor(totalCredits))
            schedules.append(thisSchedule)
            iterator += 1

        schedules.sort(key=lambda combo: combo.get_gpa())
        schedules.reverse()
        if len(schedules) > 100:
            schedules = schedules[:100]

        finalResult = Result(query_instance.classList, query_instance.blockTimes, schedules, datetime.datetime.now(), query_instance.semester)
        print(finalResult.toString())
        return finalResult


    # USE THIS QUERY FOR SEARCH

@app.route('/export', methods=['POST'])
def exporting():
    data = request.get_json()

    classes = []
    for x in data["classes"]:
        meetingInfos = [MeetingInfo(x[9][0], x[9][1], int(x[9][2]), int(x[9][2])), MeetingInfo(x[9][4], x[9][5], int(x[9][6]), int(x[9][7]))]
        
        course = Course(name=x[0], subject=x[1], courseNum=x[2], professor=x[3], CRN=x[4], projGPA=x[5], credits=x[6], modality=x[7], semester=x[8], meetingInfos=meetingInfos)
        classes.append(course)

    # Create semester start and end datetime instances
    semStart = datetime.datetime(year=2024, month=1, day=16)
    semEnd = datetime.datetime(year=2024, month=5, day=1)

    # Create a schedule instance
    best_gpa_schedule = Schedule(id=data["id"], name=data["name"], GPA=float(data["gpa"]), classes=classes, semStart=semStart, semEnd=semEnd, totalCredits=int(data["totalCredits"]))

    ics_maker.get_ics(best_gpa_schedule)
    return send_from_directory('static', 'ics_test.ics', as_attachment=True)



if __name__ == '__main__':
    app.run(debug=True)
