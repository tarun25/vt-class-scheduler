# Voti

Voti is a schedule creation tool that uses historical GPA data from Virginia Tech's Grade Distribution. The process creates a course schedule that maximizes a student's predicted GPA based on a professor's grade distribution and time of day. A user is able to change the optimization by blocking time slots and choosing the GPA assumption for new professors. After a user generates schedules, they can export their favorite into their preferred calendar application. 

## Usage

Simply go to [Voti.app](https://voti.app) and start generating schedules!

## Visuals
![Starting screen](https://i.imgur.com/ZjzMfpD.png)
![Starting screen](https://i.imgur.com/W9sV2Ny.png)


## Support

Please open an issue!

## Authors and acknowledgment
Rayan Bouhal

Tarun Giridhar

Marco G. Hauger

Samhita Reddivalam

Tammy Tran

Trevor Ziffer

## Links:

https://libdoc.dpu.ac.th/eBook/113642.pdf  

https://github.com/VirginiaTech/pyvt  2017 

https://pypi.org/project/vt-timetable/#description 2022 (works!) 

https://code.vt.edu/tarun25/vt-class-scheduler 


