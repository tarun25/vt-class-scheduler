import unittest
from flask import session
import sys
import os

# Add the parent directory to the sys.path to allow imports from there
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from app import app  # Import the Flask app

class TestFlaskRoutes(unittest.TestCase):
    def setUp(self):
        """Prepare the environment for testing.
        - Configures the app to run in testing and debug mode.
        - Initializes a test client for this app."""
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        self.client = app.test_client()

    def test_index_route(self):
        """Test the index route for correct HTTP response and page content."""
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Welcome', response.data.decode())

    def test_new_schedule_page(self):
        """Test the new schedule page loads correctly."""
        response = self.client.get('/new-schedule')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Create New Schedule', response.data.decode())

    def test_view_schedules_page(self):
        """Test the view schedules page loads and displays placeholder text."""
        response = self.client.get('/view-schedules')
        self.assertEqual(response.status_code, 200)
        self.assertIn('under construction', response.data.decode())

    def test_process_schedule_post(self):
        """Test submitting the schedule form and setting session data."""
        with self.client:
            response = self.client.post('/process-schedule', data={
                'semester': 'Fall 2024',
                'course1': 'CS 101',
                'course2': 'MATH 202'
            }, follow_redirects=True)
            self.assertEqual(response.status_code, 200)
            self.assertIn('Fall 2024', session['semester'])

    def test_block_times_get(self):
        """Test loading the block times page."""
        response = self.client.get('/block-times')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Block Times', response.data.decode())

    def test_block_times_post(self):
        """Test submitting blocked times and updating session data."""
        with self.client:
            response = self.client.post('/block-times', data={
                'blockedTimes': ['Monday-10-00', 'Wednesday-12-00']
            }, follow_redirects=True)
            self.assertEqual(response.status_code, 200)
            self.assertIn('Monday', session['blockTimes'][0]['day_of_week'])

    def test_prof_grading_page(self):
        """Test loading the professor grading preferences page."""
        response = self.client.get('/prof-grading')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Select GPA', response.data.decode())

    def test_start_generating(self):
        """Test the redirect to the generating page."""
        response = self.client.get('/start-generating')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Generating', response.data.decode())

    def test_show_results(self):
        """Test displaying results; expects no results available initially."""
        with self.client:
            session['classList'] = ['CS 1114', 'ENGL 1106']
            session['blockTimes'] = [{'day_of_week': 'Monday', 'hour': 10, 'minute': 30}]
            session['selectedGPA'] = 3.5
            session['semester'] = 'Spring 2024'
            response = self.client.get('/show-results')
            self.assertEqual(response.status_code, 404)  # No results should be stored initially


    def tearDown(self):
        """Cleanup after each test."""
        pass

if __name__ == '__main__':
    unittest.main()
