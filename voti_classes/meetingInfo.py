from datetime import datetime
class MeetingInfo:
    """
    Example usage:

    # Create a MeetingInfo object with initial day, building, hour, and minute
    meeting = MeetingInfo("Monday", "Main Building", 13, 30)

    # Get the time of the meeting, including the day
    print(meeting.get_time())  # Output: Monday 01:30 PM

    # Set a new time and day for the meeting
    meeting.set_time("Tuesday", 14, 30)

    # Get the updated time of the meeting, including the new day
    print(meeting.get_time())  # Output: Tuesday 02:30 PM

    # Print the meeting info, including the day in the time
    print(meeting)  # Output: Day: Tuesday, Building: Main Building, Time: Tuesday 02:30 PM
    """
    def __init__(self, day_of_week: str, building: str, hour:int, minute:int):
        self._day_of_week = day_of_week
        self._building = building
        self.hour = hour
        self.minute = minute
        self.time = datetime.strptime(f"{hour}:{minute}", "%H:%M").time()

    @property
    def day_of_week(self):
        return self._day_of_week

    @day_of_week.setter
    def day_of_week(self, new_day_of_week):
        self._day_of_week = new_day_of_week

    @property
    def building(self):
        return self._building

    @building.setter
    def building(self, new_building):
        self._building = new_building

    def get_time(self):
        return f"{self.day_of_week} {self.time.strftime('%I:%M %p')}"

    def set_time(self, day, hour, minute):
        self.day_of_week = day
        self.time = datetime.strptime(f"{hour}:{minute}", "%H:%M").time()

    def __str__(self):
        return f"Day: {self.day_of_week}, Building: {self.building}, Time: {self.get_time()}"
    
