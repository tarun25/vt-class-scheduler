from datetime import datetime
class Schedule:

    '''
    Example usage:
    # Create a list of meetingInfos
    meetingInfos = [
        meetingInfo("Monday", "Main Building", 13, 30),
        meetingInfo("Wednesday", "Main Building", 13, 30),
        meetingInfo("Tuesday", "Lab Building", 9, 00)
    ]

    # Create a course instance
    course_instance = course(name="Software Design and Data Structures", subject="CS", courseNum=2114, 
                             professor="Ellis, Margaret", CRN=13295, projGPA=3.4, 
                             credits=3, modality="In-Person", meetingInfos=meetingInfos)
    
    # Create a list of classes
    classes = [
        course_instance1, course_instance2, course_instance3, course_instance4, course_instance5
    ]

    # Create semester start and end datetime instances
    semStart = datetime.datetime(year=2024, month=1, day=16)
    semEnd = datetime.datetime(year=2024, month=5, day=1)

    # Create a schedule instance
    best_gpa_schedule = schedule(id=1, name="Best GPA", GPA=3.8, classes=classes, semStart=semStart, semEnd=semEnd, totalCredits=15)
    '''
    def __init__(self, id:int, name:str, GPA:float, classes:list, semStart:datetime, semEnd:datetime, totalCredits:int):
        self.id = id
        self.name = name
        self.GPA = GPA
        self.classes = classes
        self.semStart = semStart
        self.semEnd = semEnd
        self.totalCredits = totalCredits

    def toString(self)->str:
        return self.get_name() + "\nAnticipated GPA: " + "{:.2f}".format(self.get_gpa()) + " (" + str(self.get_totalCredits()) + "c)\n" + "\n".join([cls.toString() for cls in self.get_classes()])

    def get_id(self)->int:
        return self.id
    
    def get_name(self)->str:
        return self.name

    def get_gpa(self)->float:
        return self.GPA
    
    def get_classes(self)->list:
        return self.classes
    
    def get_semStart(self)->datetime:
        return self.semStart
    
    def get_semEnd(self)->datetime:
        return self.semEnd
    
    def get_totalCredits(self)->int:
        return self.totalCredits
    
    def set_id(self, id:int):
        self.id = id

    def set_name(self, name:str):
        self.name = name

    def set_gpa(self, GPA:float):
        self.GPA = GPA
    
    def set_classes(self, classes:list):
        self.classes = classes

    def set_semStart(self, semStart:datetime):
        self.semStart = semStart
    
    def set_semEnd(self, semEnd:datetime):
        self.semEnd = semEnd

    def set_totalCredits(self, totalCredits:int):
        self.totalCredits = totalCredits
    
