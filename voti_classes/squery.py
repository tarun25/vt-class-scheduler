class SQuery:
    '''
    Example usage:
    # Create a list of classes
    classList = ["Math", "Science", "History"]

    # Create a list of block times
    blockTimes = [
        MeetingInfo("Monday", "Main Building", 13, 30),
        MeetingInfo("Wednesday", "Library", 10, 45)
    ]

    # Create a new query instance
    query_instance = SQuery(classList=classList, blockTimes=blockTimes, newProfGPA=2.75, semester="Spring 2024")
    '''
    def __init__(self, classList: list, blockTimes: list, newProfGPA: float, semester: str):
        self._classList = classList
        self._blockTimes = blockTimes
        self._newProfGPA = newProfGPA  
        self._semester = semester

    @property
    def classList(self):
        return self._classList

    @classList.setter
    def classList(self, new_classList):
        self._classList = new_classList

    @property
    def blockTimes(self):
        return self._blockTimes

    @blockTimes.setter
    def blockTimes(self, new_blockTimes):
        self._blockTimes = new_blockTimes

    @property
    def newProfGPA(self):
        return self._newProfGPA  

    @newProfGPA.setter
    def newProfGPA(self, val):
        self._newProfGPA = val 
    @property
    def semester(self):
        return self._semester

    @semester.setter
    def semester(self, new_semester):
        self._semester = new_semester
