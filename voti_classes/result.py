from datetime import datetime

class Result:
    '''
    Example usage:
    # Create a list of classes
    classList = ["Math", "Science", "History"]

    # Create a list of blocked times
    blockedTimes = [
        MeetingInfo("Monday", "Main Building", 13, 30),
        MeetingInfo("Wednesday", "Library", 10, 45)
    ]

    # Create a list of schedules
    schedules = [
        {"Monday": "Math", "Wednesday": "Science"},
        {"Monday": "History", "Wednesday": "Math"}
    ]

    # Create a Result instance
    result_instance = Result(classList=classList, blockedTimes=blockedTimes, schedules=schedules, dateGenerated=datetime.now(), semester="Spring 2024")
    '''
    def __init__(self, classList:list,  blockedTimes:list, schedules: list, dateGenerated: datetime, semester:str):
        self._classList = classList
        self._blockedTimes = blockedTimes
        self._schedules = schedules
        self._dateGenerated = dateGenerated
        self._semester = semester

    @property
    def classList(self):
        return self._classList

    @classList.setter
    def classList(self, new_classList):
        self._classList = new_classList

    @property
    def blockedTimes(self):
        return self._blockedTimes

    @blockedTimes.setter
    def blockedTimes(self, new_blockedTimes):
        self._blockedTimes = new_blockedTimes

    @property
    def schedules(self):
        return self._schedules

    @schedules.setter
    def schedules(self, new_schedules):
        self._schedules = new_schedules

    @property
    def dateGenerated(self):
        return self._dateGenerated

    @dateGenerated.setter
    def dateGenerated(self, new_dateGenerated):
        self._dateGenerated = new_dateGenerated

    @property
    def semester(self):
        return self._semester

    @semester.setter
    def semester(self, new_semester):
        self._semester = new_semester

    def toString(self)->str:
        return "\n\n".join([sch.toString() for sch in self.schedules])
    
