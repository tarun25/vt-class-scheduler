from vtt import *
import csv

fields = ["Year", "Semester", "CRN", "Subject", "Code", "Name", "Type", "Modality", 
          "Credit Hours", "Capacity", "Professor", "MeetTime 1", "MeetTime 2", "MeetTime 3", "MeetTime 4"]
rows = []

for subject in get_subjects():
    print(subject[0])
    courses = search_timetable('2024', Semester.FALL, subject=subject[0])
    for course in courses:
        temp = [course.get_year(), course.get_semester().name, course.get_crn(), course.get_subject(), course.get_code(), course.get_name(), course.get_type().name, course.get_modality(), course.get_credit_hours(), course.get_capacity(), course.get_professor()]
        dictionary = course.get_schedule()
        for key, value in dictionary.items():
            st = ""
            st = key.value
            st += ", "
            st += (str(value))
            temp.append(st)
        rows.append(temp)

filename = "timetable.csv"

with open(filename, 'w', newline='') as csvfile:
    # creating a csv writer object
    csvwriter = csv.writer(csvfile)
    # writing the fields
    csvwriter.writerow(fields)
    # writing the data rows
    csvwriter.writerows(rows)
