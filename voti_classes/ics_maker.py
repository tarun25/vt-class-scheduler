from ics import Calendar, Event
import calendar
from voti_classes import Schedule, Course, MeetingInfo
from dateutil.rrule import rrule, WEEKLY
from datetime import datetime, time, timedelta
import pytz


'''
Example usage:

meetingInfos = [
    MeetingInfo("Monday", "Main Building", 13, 30),
    MeetingInfo("Wednesday", "Main Building", 13, 30),
    MeetingInfo("Friday", "Lab Building", 9, 00)
]

# Create a course instance
course_instance = Course(name="Software Design and Data Structures", subject="CS", courseNum=2114, 
                         professor="Ellis, Margaret", CRN=13295, projGPA=3.4, 
                         credits=3, modality="In-Person", semester="Spring 2024" ,meetingInfos=meetingInfos)

# Create a list of classes
classes = [
    course_instance
]

# Create semester start and end datetime instances
semStart = datetime(year=2024, month=1, day=16)
semEnd = datetime(year=2024, month=5, day=1)

# Create a schedule instance
best_gpa_schedule = Schedule(id=1, name="Best GPA", GPA=3.8, classes=classes, semStart=semStart, semEnd=semEnd, totalCredits=15)

get_ics(best_gpa_schedule)
'''

local_tz = pytz.timezone('America/New_York')

def get_ics(result: Schedule):
    cal = Calendar()
    for clss in result.classes:

        sem_start = result.get_semStart()
        sem_end = result.get_semEnd()

        days = []
        hours = []
        minutes = []
        for meet in clss.meetingInfos:
            hours.append(meet.time.hour)
            minutes.append(meet.time.minute)
            if (meet.day_of_week[:2].upper() == "MO"):
                days.append(0)
                print(meet.time.hour)
            elif (meet.day_of_week[:2].upper() == "TU"):
                days.append(1)
            elif (meet.day_of_week[:2].upper() == "WE"):
                days.append(2)
            elif (meet.day_of_week[:2].upper() == "TH"):
                days.append(3)
            elif (meet.day_of_week[:2].upper() == "FR"):
                days.append(4)


        rule = rrule(WEEKLY, byweekday=(days), until=sem_end, dtstart=sem_start)
        
        count = 0
        for dt in rule:
            timez = time(hour=hours[count], minute=minutes[count])
            newdt = datetime.combine(dt.date(), timez, tzinfo=local_tz).astimezone(local_tz)

            diff = timedelta(minutes=50)
            enddt = (newdt) + diff

            count = (count + 1) % len(hours)
            print(newdt)
            print(enddt)
            print("")
            event = Event()
            event.name = clss.name
            event.begin = newdt
            event.end = enddt
            cal.events.add(event)

    with open ('static/ics_test.ics', 'w') as f:
        f.writelines(cal.serialize_iter())





        
