
class Course:

    '''
    Example usage:
    # Create a list of meetingInfos
    meetingInfos = [
        meetingInfo("Monday", "Main Building", 13, 30),
        meetingInfo("Wednesday", "Main Building", 13, 30),
        meetingInfo("Tuesday", "Lab Building", 9, 00)
    ]

    # Create a course instance
    course_instance = course(name="Software Design and Data Structures", subject="CS", courseNum=2114, 
        professor="Ellis, Margaret", CRN=13295, projGPA=3.4, credits=3, modality="In-Person", semester="Spring 2024", meetingInfos=meetingInfos)
    
    '''
    def __init__(self, name:str, subject:str, courseNum:int, professor:str, CRN:int, projGPA:float, credits:int, modality:str, semester:str, meetingInfos:list):
        self.name = name
        self.subject = subject
        self.courseNum = courseNum
        self.professor = professor
        self.CRN = CRN
        self.projGPA = projGPA
        self.credits = credits
        self.modality = modality
        self.meetingInfos = meetingInfos
        self.semester = semester


    def toString(self)->str:
        return self.get_courseCode() + " (" + str(self.get_crn()) + ", " + self.get_professor() + ") GPA: " + "{:.2f}".format(self.get_projGPA())
    
    def get_name(self)->str:
        return self.name
    
    def get_subject(self)->str:
        return self.subject
    
    def get_courseNum(self)->int:
        return self.courseNum

    def get_professor(self)->str:
        return self.professor
    

    def get_crn(self)->int:
        return self.CRN
    

    def get_projGPA(self)->float:
        return self.projGPA
    

    def get_credits(self)->int:
        return self.credits
    

    def get_modality(self)->str:
        return self.modality
    

    def get_meetingInfos(self)->list:
        return self.meetingInfos
    

    def set_name(self, name:str):
        self.name = name    
    
    def set_subject(self, subject:str):
        self.subject = subject
    
    def set_courseNum(self, courseNum:int):
        self.courseNum = courseNum

    def set_professor(self, professor:str):
        self.professor = professor
    
    def get_courseCode(self)->str:
        return self.subject + " " + str(self.courseNum)
    
    def get_semester(self)->str:
        return self.semester

    def set_semester(self, semester:str):
        self.semester = semester

    def set_crn(self, CRN:int):
        self.CRN = CRN
    

    def set_projGPA(self, projGPA:float):
        self.projGPA = projGPA
    

    def set_credits(self, credits:int):
        self.credits = credits
    

    def set_modality(self, modality:str):
        self.modality = modality
    

    def set_meetingInfos(self, meetingInfos:list):
        self.meetingInfos = meetingInfos

